<?php

/**
 * @file
 * commerce_checkout_page_complete.rules.inc
 */

/**
 * Implements hook_rules_event_info().
 */ 
function commerce_checkout_complete_page_rules_event_info() {
  $events = array();

  $events['commerce_checkout_complete_page_is_viewed'] = array(
    'label' => t('Checkout complete page is viewed'),
    'group' => t('Commerce Checkout'),
    'variables' => array(
      'commerce_order' => array(
      'type' => 'commerce_order',
      'label' => t('Completed order', array(), array('context' => 'a drupal commerce order')),
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  return $events;
  
}

